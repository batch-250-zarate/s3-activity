package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int i, num = 1;
        int factorialNumber = 0;

        System.out.println("Input an integer whose factorial will be computed: ");

        try {
            factorialNumber = in.nextInt();

            if (factorialNumber < 0) {
                System.out.println("factorialNumber must be > 0 or = to 0.");
            } else {
                for (i = 1; i <= factorialNumber; i++) {
                    num *= i;
                }
                System.out.println("Factorial of " + factorialNumber + " is: " + num);
            }

        } catch (Exception e) {
            System.out.println("Invalid Input. Please enter a number.");
            e.printStackTrace();
        } finally {
            System.out.println("You have entered: " + factorialNumber);
        }
    }
}
